require 'carrotgrenade'

require 'erb'
require 'set'

module CarrotGrenade

	# Interface for configuration, and generation of cards.
	#
	# @api public
	class Deck

		include Enumerable

    # :nodoc:
    # @api private
    attr_reader :card_width, :card_height, :page_width, :page_height

    # CarrotGrenade's constructor that accepts an immutable configuration source html
    #
    # You will want to process a different html for each card type variant
    # @example
    #   require 'carrotgrenade'
    #   d = CarrotGrenade::Deck.new card: 'some/path/to/a/card.html'
    #
    # @param card [String] path to card html template
    # @api public
    def initialize(card:, output: "_output")
      @card_template = card
      @card_width = 0
      @card_height = 0
      @page_width = 0
      @page_height = 0
      @output_root = output
      generate
    end

    # :nodoc:
    # @api private
    def generate

    	html_output_path = File.absolute_path "#{@output_root}/#{File.basename(@card_template)}"

    	output_consolidated_css = "#{File.basename(@card_template, ".*")}.css"
    	css_output_path = File.absolute_path "#{@output_root}/css/#{output_consolidated_css}"

    	asset_input_path = File.absolute_path "#{File.join(File.dirname(@card_template), "assets")}"
    	asset_output_path = File.absolute_path "#{@output_root}"

  		data_source_scraper = ProcessorDataSourceScraper.new
			data_source_scraper.load! @card_template
			data_source = data_source_scraper.process!

			rhtml = ProcessorRHTML.new
			rhtml.load! @card_template

			default_css_path = File.expand_path "./css/card.css", File.dirname(__FILE__)

			template_dir = File.dirname(File.absolute_path @card_template)

    	Dir.chdir template_dir do

	    	CarrotGrenade::logger.info "CarrotGrenade v#{CarrotGrenade::VERSION}"
	    	CarrotGrenade::logger.info "  processing #{@card_template} to #{html_output_path}"

				table = nil

				if /.*docs\.google\.com\/spreadsheets.*/.match data_source 
					csv = ProcessorGSCSV.new
					csv.load! data_source
					table = csv.process!
				else
					csv = File.open(data_source).read
					table = CSV.parse csv, :headers => true
				end

				# setup core styling
				head = Set.new
				head.add "<link rel=\"stylesheet\" href=\"#{default_css_path}\" type=\"text/css\">"

				body = Array.new

				table.each do |card|
					
					card_binding = binding

					1.upto(card['Count'].to_i) do

						templated = rhtml.process! context: card_binding

						head_content = get_html_content_between html: templated, start: "<head>", finish: "</head>"
						head_content.split("\n").each do |entry|
							head.add entry.strip
						end

						body_content = get_html_content_between html: templated, start: "<body>", finish: "</body>"
						body.push body_content.strip
						
					end

				end

				css = ProcessorCSS.new

				head.map! do |head_content|
					sanitized_head_content = head_content
					matcher = /<link[^>]*?href="([^"]+\.css)"/.match head_content
					if matcher
						matcher.captures.each do |capture|
							if /.*http.*/.match capture
								css.load! capture
							else
								css.load! File.absolute_path(capture)
							end
						end
						sanitized_head_content = ""
					end
					sanitized_head_content
				end

				consolidated_css = css.process!

				page_css = consolidated_css["@page"]
				page_width_style, page_height_style = page_css["size"].split(" ")[0..-1]
				@page_width, page_width_unit_type = get_value_unit(page_width_style)
				@page_height, page_height_unit_type = get_value_unit(page_height_style)

				card_css = consolidated_css[".card"]
				@card_width, card_width_unit_type = get_value_unit(card_css["width"])
				@card_height, card_height_unit_type = get_value_unit(card_css["height"])
				card_margin_top, card_margin_top_unit_type = get_value_unit(card_css["margin-top"])
				card_margin_left, card_margin_left_unit_type = get_value_unit(card_css["margin-left"])

				if card_width_unit_type != card_height_unit_type or card_width_unit_type != card_margin_top_unit_type or card_width_unit_type != card_margin_left_unit_type or card_width_unit_type != page_width_unit_type or card_width_unit_type != page_height_unit_type
					raise "Inconsistent units in .card css: #{card_width_unit_type} #{card_height_unit_type} #{card_margin_top_unit_type} #{card_margin_left_unit_type} and/or @page css: #{page_width_unit_type} #{page_height_unit_type}"
				end

				size_unit_type = card_width_unit_type
				to_unit = 1.0

				result = "<!DOCTYPE html><html lang=\"en\"><head><link rel=\"stylesheet\" href=\"./css/#{output_consolidated_css}\" type=\"text/css\">"

				head.each do |head_content|
					result += head_content
				end

				result += "</head><body style=\"margin: 0\">\n"

				card_margin_top_unit = card_margin_top * to_unit
				card_margin_left_unit = card_margin_left * to_unit

				card_width_unit = @card_width * to_unit
				card_height_unit = @card_height * to_unit

				page_width_unit = @page_width * to_unit
				page_height_unit = @page_height * to_unit

				cards_per_row = (page_width_unit / (card_width_unit + card_margin_left_unit)).floor

				row_width_unit = cards_per_row * (card_width_unit + card_margin_left_unit) + card_margin_left_unit
				row_height_unit = card_height_unit + card_margin_top_unit

				rows_per_page = (page_height_unit / (row_height_unit + card_margin_top_unit)).floor

				CarrotGrenade::logger.info "  building #{card_width_unit}#{size_unit_type} x #{card_height_unit}#{size_unit_type} cards"
				CarrotGrenade::logger.info "  on a #{page_width_unit}#{size_unit_type} x #{page_height_unit}#{size_unit_type} page"
				CarrotGrenade::logger.info "  with #{row_width_unit}#{size_unit_type} x #{row_height_unit}#{size_unit_type} rows"
				CarrotGrenade::logger.info "  at #{cards_per_row} cards per row and #{rows_per_page} rows per page"

				open_page = "\n<div style=\"page-break-inside: avoid; width: #{page_width_unit}#{size_unit_type}; height: #{page_height_unit}#{size_unit_type};\">"
				close_page = "\n</div>"

				open_row = "\n  <div style=\"width: #{row_width_unit}#{size_unit_type}; height: #{row_height_unit}#{size_unit_type};\">"
				close_row = "\n  </div>"

				open_card = "\n    <div>\n"
				close_card = "\n    </div>"

				result += build_cards_html cards: body.each, rows_per_page: rows_per_page, cards_per_row: cards_per_row, open_page: open_page, close_page: close_page, open_row: open_row, close_row: close_row, open_card: open_card, close_card: close_card

				result += "\n</body></html>"

				dir = File.dirname(html_output_path)

			  unless File.directory?(dir)
			    FileUtils.mkdir_p(dir)
			  end

				File.open(html_output_path, "w+") do |rendered|
					rendered.puts result
				end

				dir = File.dirname(css_output_path)

			  unless File.directory?(dir)
			    FileUtils.mkdir_p(dir)
			  end

				File.open(css_output_path, "w+") do |consolidated_css_file|
					consolidated_css.each do |css_selector, css_rule|
						css_entry = css_rule.to_s
						if not css_entry.start_with? css_selector
							css_entry = "#{css_selector} #{css_entry}"
						end
						consolidated_css_file.puts css_entry
					end
				end

				FileUtils.cp_r asset_input_path, asset_output_path
			end

    end

    # :nodoc:
    # @api private
    def get_value_unit(css_measure)
    	sanitized_css = css_measure.gsub(';','').strip
    	return (sanitized_css[0..-3]).to_f, sanitized_css[-2..-1]
    end

    # :nodoc:
    # @api private
    def build_cards_html(cards:, rows_per_page:, cards_per_row:, open_page:, close_page:, open_row:, close_row:, open_card:, close_card:)
    	result = ""
    	page = ""
    	row = ""

    	begin
    		while true do
    			page = open_page

					1.upto rows_per_page do
						row = open_row

						1.upto cards_per_row do
							cards.peek
							row += open_card + cards.next + close_card
						end
						
						page += row + close_row
						row = ""
					end

					result += page + close_page
					page = ""
    		end
    	rescue StopIteration
    		if row.length > open_row.length
  				page += row + close_row
  			end
    		if page.length > open_page.length
    			result += page + close_page
    		end
    	end

    	return result
		end

    # :nodoc:
    # @api private
		def get_html_content_between(html: nil, start: nil, finish: nil)
			result = html.split(start)[1]
			if result
				result = result.split(finish)[0]
			end
			if !result
				result = ""
			end
			return result
    end

	end

end