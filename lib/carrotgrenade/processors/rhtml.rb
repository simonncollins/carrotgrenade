require 'carrotgrenade'

require 'erb'

module CarrotGrenade

	# Renders an rHTML template file into HTML with a given binding
	#
	# @api private
	class ProcessorRHTML

		# :nodoc:
    # @api private
    attr_reader :rhtml, :import_scraper, :modified, :structure

		# :nodoc:
    # @api private
		def initialize
			@rhtml = ""
			@import_scraper = nil
			@modified = true
			@structure = ""
		end

    # Loads an rHTML in memory
    #
    # @param source [String] path to rHTML file
    # @api private
		def load!(source)
			@rhtml = File.open(source).read
			@import_scraper = ProcessorImportScraper.new
			@import_scraper.load! source
			@modified = true
			@structure = ""
		end

    # Renders an rHTML template into HTML
    #
    # @param context [Binding] binding context for template rendering
    # @api private
		def process!(context: nil)
			if @modified
				resolved = @rhtml

				imports = import_scraper.process!
				imports.each do |k, v|
					resolved.gsub! k, File.open(v).read
				end

				@structure = ERB.new(resolved).result context
			end
			return @structure
		end
	end
end