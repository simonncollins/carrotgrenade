require 'carrotgrenade'

require 'open-uri'
require 'csv'

module CarrotGrenade

	# Fetches and loads a Google Sheets document and parses it into a CSV::Table
	#
	# @api private
	class ProcessorGSCSV

		# :nodoc:
    # @api private
    attr_reader :csv, :structure, :modified

		# :nodoc:
    # @api private
		def initialize
			@csv = nil
			@structure = nil
			@modified = true
		end

    # Loads a Google Sheets document as a CSV in memory
    #
    # @param source [String] path to CSS file
    # @api private
		def load!(source)
			@modified = true
			id, gid = source.split('https://docs.google.com/spreadsheets/d/')[1].split('/edit#gid=')
			@csv = open("https://docs.google.com/spreadsheets/d/#{id}/export?format=csv&id=#{id}&gid=#{gid}", :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE).read
		end

    # Parses a CSV in memory into a CSV::Table
    #
    # @api private
		def process!
			if @modified
				@structure = CSV.parse @csv, :headers => true
				@modified = false
			end
			return @structure
		end
	end
end