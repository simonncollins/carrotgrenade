require 'carrotgrenade'

module CarrotGrenade

	# Scrapes a source file for a CarrotGrenadeDataSource entry
	#
	# @api private
	class ProcessorDataSourceScraper

		# :nodoc:
    # @api private
    attr_reader :text

		# :nodoc:
    # @api private
		def initialize
			@text = nil
		end

    # Loads a source file in memory
    #
    # @param source [String] path to source file
    # @api private
		def load!(source)
			@text = File.open(source).read
		end

    # Matches the first instance of a CarrotGrenadeDataSource entry
    #
    # @api private
		def process!
			result = ""
			matcher = /<!-- CarrotGrenadeDataSource=(.+) -->/.match @text
			if matcher
				# we only handle 1 DataSource per card
				result = matcher.captures[0]
			end
			return result
		end
	end
end