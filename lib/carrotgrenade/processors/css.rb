require 'carrotgrenade'

require 'css_parser'

module CarrotGrenade

	# Consolidates multiple CSS sources into a hash of selector keys to CssParser::RuleSet's
	#
	# @api private
	class ProcessorCSS

		# :nodoc:
    # @api private
    attr_reader :parser, :structure, :modified

		# :nodoc:
    # @api private
		def initialize
			@parser = CssParser::Parser.new
			@structure = nil
			@modified = true
		end

    # Loads a CSS file and parses its contents
    #
    # @param source [String] path to CSS file
    # @api private
		def load!(source)
			@modified = true
			if /.*http.*/.match source
				@parser.load_uri! source
			else
				@parser.load_string! File.open(source).read
			end
		end

    # Consolidates parsed CSS into a hash of selector keys to CssParser::RuleSet's 
    #
    # @api private
		def process!
			if @modified
				@structure = Hash.new
				rule_sets_by_selector = Hash.new
				@parser.each_rule_set do |rule_set, media_types|
					rule_set.each_selector do |selector, declaration, specificity|
						if not rule_sets_by_selector.has_key? selector
							rule_sets_by_selector[selector] = Array.new
						end
						rule_sets_by_selector[selector].push rule_set
					end
				end

				rule_sets_by_selector.each do |selector, rule_sets|
					@structure[selector] = CssParser.merge rule_sets
				end
				@modified = false
			end
			return @structure
		end

		def to_s
			return @parser.to_s
		end
	end
end