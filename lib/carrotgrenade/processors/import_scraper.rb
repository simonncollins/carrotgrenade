require 'carrotgrenade'

module CarrotGrenade

	# Scrapes a source file for a CarrotGrenadeImport entry
	#
	# @api private
	class ProcessorImportScraper

		# :nodoc:
    # @api private
    attr_reader :text, :structure, :modified

		# :nodoc:
    # @api private
		def initialize
			@text = ""
			@structure = {}
			@modified = true
		end

    # Loads a source file in memory
    #
    # @param source [String] path to source file
    # @api private
		def load!(source)
			@text = File.open(source).read		
			@structure = {}
			@modified = true
		end

    # Matches all import instances and creates key/val for import statement to importable file path
    #
    # @api private
		def process!
			if @modified
				@text.scan(/(<!-- CarrotGrenadeImport=)(.+)( -->)/).each do |import|
					@structure["#{import[0]}#{import[1]}#{import[2]}"] = import[1]
				end
			end
			return @structure
		end
	end
end