require 'fileutils'

module CarrotGrenade
  # CarrotGrenade's command-line options
  module Commands

    # Explode card templates into rendered printable pages
    #
    # @api public
    class Explode

      # :nodoc:
      # @api private
      def process(sources)
        source_suffix = ".html"

        resolved_sources = Array.new

        if sources.empty?
          sources = Dir.glob File.join("source", "*#{source_suffix}")
        end

        sources.each do |source|
          if File.directory? source
            resolved_sources |= Dir.glob File.join(source, "*#{source_suffix}")
          elsif File.file? source and source.end_with? source_suffix
            resolved_sources |= [source]
          else
            if not source.end_with? source_suffix
              if not source.end_with? "*"
                source += "*"
              end
              source += source_suffix
            end
            resolved_sources |= Dir.glob source
          end
        end

        if resolved_sources.empty?
          $stderr.puts "No *.html files found, aborting."
        else
          resolved_sources.each do |resolved_source|
            d = CarrotGrenade::Deck.new card: resolved_source
          end
        end
      end

    end
  end
end