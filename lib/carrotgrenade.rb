require 'logger'

require 'carrotgrenade/version'
require 'carrotgrenade/deck'
require 'carrotgrenade/commands/new'
require 'carrotgrenade/commands/explode'
require 'carrotgrenade/processors/css'
require 'carrotgrenade/processors/google_sheets_csv'
require 'carrotgrenade/processors/rhtml'
require 'carrotgrenade/processors/data_source_scraper'
require 'carrotgrenade/processors/import_scraper'

# The project module
#
# @api public
module CarrotGrenade

  # Access the internal logger that CarrotGrenade uses. By default, CarrotGrenade configure the logger to the INFO level
  # Use this to suppress or increase output levels.
  # @example
  #   CarrotGrenade.logger.level = Logger::DEBUG #show waaaay more information than you probably need, unless you're a dev
  #   CarrotGrenade.logger.level = Logger::ERROR #basically turns it off
  #
  # @return [Logger] the ruby logger
  # @api public
  def logger
    if @logger.nil?
      @logger = Logger.new($stdout);
      @logger.level = Logger::INFO;
      @logger.formatter = proc do |severity, datetime, m_progname, msg|
        "#{datetime} #{severity}: #{msg}\n"
      end
    end
    @logger
  end
  module_function :logger

end