# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'carrotgrenade/version'

Gem::Specification.new do |s|
	s.specification_version = 2 if s.respond_to? :specification_version=
  s.required_rubygems_version = Gem::Requirement.new('>= 0') if s.respond_to? :required_rubygems_version=
  s.rubygems_version = '2.2.2'
  s.required_ruby_version = '>= 2.0.0'

	s.name				= 'carrotgrenade'
	s.version			= CarrotGrenade::VERSION
	s.summary			= "Card Generator"
	s.description = "Generate Decks of Cards using HTML Templating"
	s.authors			= ["Simon Collins"]
	s.email				= 'simonncollins+carrotgrenade@gmail.com'
	s.homepage		= 'http://rubygems.org/gems/carrotgrenade'
	s.license     = 'MIT'

	s.extra_rdoc_files	= Dir['README.md']
	s.files         		= `git ls-files -z`.split("\x0")
  s.executables   		= s.files.grep(/^bin\//) { |f| File.basename(f) }
  s.test_files    		= s.files.grep(/^(spec)\//)
  s.require_paths 		= ['lib']

  s.add_runtime_dependency 'css_parser'
  s.add_runtime_dependency 'mercenary'

  s.add_development_dependency 'bundler', '~> 1.6'
  s.add_development_dependency 'rake'
  s.add_development_dependency 'rspec', '~> 3.1'
  s.add_development_dependency 'rspec-mocks', '~> 3.1'
  s.add_development_dependency 'coveralls'
  s.add_development_dependency 'yard', '~> 0.8'
  s.add_development_dependency 'redcarpet', '~> 3.1'
end