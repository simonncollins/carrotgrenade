require 'spec_helper'

require 'carrotgrenade'

describe  CarrotGrenade::Commands::Explode do

  describe '#process' do
    before(:all) do
      @old_stderr = $stderr
      $stderr = StringIO.new
      root_dir = "spec"
      @source_dir = "source"
      @output_dir = "_output"
      @oldpwd = Dir.pwd
      Dir.chdir root_dir
    end

    before(:each) do
      FileUtils.rm_rf @output_dir, secure: true
      @cmd = CarrotGrenade::Commands::Explode.new
    end

    it 'processes specified html source' do
      expect{@cmd.process(["#{@source_dir}/basic.html"])}.not_to raise_error
      expect(File.exists? "#{@output_dir}/basic.html").to be true
      expect(File.exists? "#{@output_dir}/still_basic.html").to be false
    end

    it 'processes all html sources under a dir' do
      expect{@cmd.process(["#{@source_dir}"])}.not_to raise_error
      expect(File.exists? "#{@output_dir}/basic.html").to be true
      expect(File.exists? "#{@output_dir}/still_basic.html").to be true
    end

    it 'processes all html sources matching a glob string' do
      expect{@cmd.process(["#{@source_dir}/still*"])}.not_to raise_error
      expect(File.exists? "#{@output_dir}/basic.html").to be false
      expect(File.exists? "#{@output_dir}/still_basic.html").to be true
    end

    it 'processes all html sources under source dir when none specified' do
      expect{@cmd.process([])}.not_to raise_error
      expect(File.exists? "#{@output_dir}/basic.html").to be true
      expect(File.exists? "#{@output_dir}/still_basic.html").to be true
    end

    it 'aborts when non html sources specified' do
      expect{@cmd.process(["#{@source_dir}/basic.blah"])}.not_to raise_error
      $stderr.rewind
      expect($stderr.string.chomp).to end_with "No *.html files found, aborting."
    end

    it 'aborts when no html sources found' do
      expect{@cmd.process(["#{@source_dir}/*blah*.html"])}.not_to raise_error
      $stderr.rewind
      expect($stderr.string.chomp).to end_with "No *.html files found, aborting."
    end

    after(:all) do
      FileUtils.rm_rf @output_dir, secure: true
      Dir.chdir @oldpwd
      $stderr = @old_stderr
    end
  end

end