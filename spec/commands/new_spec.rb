require 'spec_helper'

require 'carrotgrenade'
require 'fileutils'

describe  CarrotGrenade::Commands::New do

  describe '#process' do

    before(:all) do
      @old_stderr = $stderr
      $stderr = StringIO.new
      @temp_dir = "spec/_output"
      @test_dir = "#{@temp_dir}/foo"
    end

    before(:each) do
      FileUtils.rm_rf @temp_dir, secure: true
      @cmd = CarrotGrenade::Commands::New.new
    end

    it 'raises an error if no directory was specified' do
      expect{@cmd.process([])}.to raise_error(ArgumentError, "Please specify a path.")
    end

    it 'creates a new template on an fresh directory' do
      @cmd.process [@test_dir]
      expect(File.exists?("#{@test_dir}/source/basic.html")).to be true
    end

    it 'creates a new template on an empty directory' do
      FileUtils.mkdir_p @test_dir
      @cmd.process [@test_dir]
      expect(File.exists?("#{@test_dir}/source/basic.html")).to be true
    end

    it 'does not create a new template on a non-empty directory' do
      FileUtils.mkdir_p @test_dir
      File.new("#{@test_dir}/somefile.blah", "w+").close
      @cmd.process [@test_dir]
      $stderr.rewind
      expect($stderr.string.chomp).to end_with " exists and is not empty. Doing nothing and quitting."
    end

    after(:all) do
      FileUtils.rm_rf @temp_dir, secure: true
      $stderr = @old_stderr
    end
  end

end