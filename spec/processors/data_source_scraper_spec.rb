require 'spec_helper'

require 'carrotgrenade'

describe CarrotGrenade::ProcessorDataSourceScraper do

	it 'scrapes a single google spreadsheet data source from card html' do
		source = "spec/source/basic.html"
		target = "https://docs.google.com/spreadsheets/d/1M_dQPwm1Uaj0t3osCSRgEr6cv3UfGTDAZs-LEgQ7twU/edit#gid=2015050527"

		p = CarrotGrenade::ProcessorDataSourceScraper.new
		p.load! source
		match = p.process!

		expect(match).to eq target
	end

	it 'scrapes a single local data source from card html' do
		source = "spec/source/still_basic.html"
		target = "./local_data.txt"

		p = CarrotGrenade::ProcessorDataSourceScraper.new
		p.load! source
		match = p.process!

		expect(match).to eq target
	end

end