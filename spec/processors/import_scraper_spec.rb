require 'spec_helper'

require 'carrotgrenade'

describe CarrotGrenade::ProcessorImportScraper do

	before(:all) do
		@olddir = Dir.pwd
		@testdir = "spec/source"
		Dir.chdir @testdir
	end

	it 'scrapes multiple imports from a card html and loads it correctly' do
		source = "basic.html"
		imports = ["./imports/shared.ihtml", "./imports/other_shared.ihtml"]
		target = {
			"<!-- CarrotGrenadeImport=#{imports[0]} -->" => imports[0],
			"<!-- CarrotGrenadeImport=#{imports[1]} -->" => imports[1]
		 }

		p = CarrotGrenade::ProcessorImportScraper.new
		p.load! source
		match = p.process!

		expect(match).to eq target
	end

	after(:all) do
		Dir.chdir @olddir
	end

end