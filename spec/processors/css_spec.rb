require 'spec_helper'

require 'carrotgrenade'

describe CarrotGrenade::ProcessorCSS do

	it 'loads a local relative css path' do
		p = CarrotGrenade::ProcessorCSS.new
		p.load! "./spec/source/css/card.css"
		expect(p.process!['.card']).not_to be_nil
	end

	it 'loads a local absolute css path' do
		p = CarrotGrenade::ProcessorCSS.new
		p.load! File.expand_path "../../source/css/card.css", __FILE__
		expect(p.process!['.card']).not_to be_nil
	end

	it 'loads a remote css path' do
		p = CarrotGrenade::ProcessorCSS.new
		p.load! "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"
		expect(p.process!['.container']).not_to be_nil
	end

	it 'consolidates css from multiple sources' do
		p = CarrotGrenade::ProcessorCSS.new
		p.load! "./spec/source/css/card.css"
		p.load! "./spec/source/css/other_card.css"
		expect(p.process!['.card']['float'] ).to eq "left;"
		expect(p.process!['.card']['margin-top'] ).to eq "0.1cm;"
	end

	it 'cascades ordered css from multiple sources' do
		p = CarrotGrenade::ProcessorCSS.new
		p.load! "./spec/source/css/card.css"
		p.load! "./spec/source/css/other_card.css"
		expect(p.process!['.card']['background'] ).to eq "#0f0;"

		q = CarrotGrenade::ProcessorCSS.new
		q.load! "./spec/source/css/other_card.css"
		q.load! "./spec/source/css/card.css"
		expect(q.process!['.card']['background'] ).to eq "#f0f;"
	end

end