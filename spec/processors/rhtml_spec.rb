require 'spec_helper'

require 'carrotgrenade'

describe CarrotGrenade::ProcessorRHTML do

	before(:all) do
		@olddir = Dir.pwd
		@testdir = "spec/source"
		Dir.chdir @testdir
	end

	it 'loads an rhtml source and renders it according to provided context' do
		source = "./still_basic.html"
		name_value = "testme"
		card = { 'Name' => name_value }

		target = File.open(source).read.gsub("<%= card['Name'] %>", name_value)

		p = CarrotGrenade::ProcessorRHTML.new
		p.load! source
		html = p.process! context: binding

		expect(html).not_to be_nil
		expect(html).to eq target
	end

	it 'loads an rhtml source with imports and renders it according to provided context' do
		source = "./basic.html"
		test_value = "testme"
		loading_value = "row2"
		card = { 
			"Test" => test_value,
			"Loading" => loading_value
		}

		imports = ["./imports/other_shared.ihtml", "./imports/shared.ihtml"]
		import_target = {
			"<!-- CarrotGrenadeImport=#{imports[0]} -->" => File.open(imports[0]).read,
			"<!-- CarrotGrenadeImport=#{imports[1]} -->" => "<!--  -->" # shared has no evaluated content to replace import with
		 }

		target = File.open(source).read
		
		import_target.each do |k, v|
			target.gsub! k, v
		end

		target
			.gsub!("<%= card[\"Test\"] %>", test_value)
			.gsub!("<%= card[\"Loading\"] %>", loading_value)
			.gsub!("<%= iconify(\"(C)\") %>", "<img class=\"carrot\" />")
			.gsub!("<%= card[\"Loading\"].include?(\"row2\") ? \"green\" : \"red\" %>", "green")

		p = CarrotGrenade::ProcessorRHTML.new
		p.load! source
		html = p.process! context: binding

		expect(html).not_to be_nil
		expect(html).to eq target
	end

	after(:all) do
		Dir.chdir @olddir
	end

end