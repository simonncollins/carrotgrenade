require 'spec_helper'

require 'carrotgrenade'

describe CarrotGrenade::ProcessorGSCSV do

	it 'loads a google sheet into a CSV table' do
		p = CarrotGrenade::ProcessorGSCSV.new
		p.load! "https://docs.google.com/spreadsheets/d/1M_dQPwm1Uaj0t3osCSRgEr6cv3UfGTDAZs-LEgQ7twU/edit#gid=2015050527"
		t = p.process!
		expect(t).not_to be_empty
		expect(t[1]['Parsing']).to eq "row2col2"
	end

end