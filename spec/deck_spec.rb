require 'spec_helper'

require 'carrotgrenade'
require 'fileutils'

describe CarrotGrenade::Deck do

  before(:all) do
  	@in_dir = "spec/source"
    @out_dir = "spec/_output"
  end

	before(:each) do
		FileUtils.rm_rf @out_dir, secure: true
	end

	it 'accepts a card configuration' do
		d = CarrotGrenade::Deck.new card: "#{@in_dir}/basic.html", output: @out_dir
		expect(d.card_width).to eq 6.3
		expect(d.card_height).to eq 8.8
	end

	it 'allows overriding of default card configuration' do 
		d = CarrotGrenade::Deck.new card: "#{@in_dir}/still_basic.html", output: @out_dir
		expect(d.card_width).to eq 4
		expect(d.card_height).to eq 3
	end

	it 'allows overriding of default page configuration' do 
		d = CarrotGrenade::Deck.new card: "#{@in_dir}/still_basic.html", output: @out_dir
		expect(d.page_width).to eq 10
		expect(d.page_height).to eq 6
	end

	it 'produces output products for input card using google spreadsheet data' do
		d = CarrotGrenade::Deck.new card: "#{@in_dir}/basic.html", output: @out_dir
		expect(File.exist? "#{@out_dir}/basic.html").to be true
		expect(File.exist? "#{@out_dir}/css/basic.css").to be true
	end

	it 'produces output products for input card using local data' do
		d = CarrotGrenade::Deck.new card: "#{@in_dir}/still_basic.html", output: @out_dir
		expect(File.exist? "#{@out_dir}/still_basic.html").to be true
		expect(File.exist? "#{@out_dir}/css/still_basic.css").to be true
	end

	it 'copies all assets to ouput assets' do
		d = CarrotGrenade::Deck.new card: "#{@in_dir}/basic.html", output: @out_dir
		expect(File.exist? "#{@out_dir}/assets/img/carrot.png").to be true
		expect(File.exist? "#{@out_dir}/assets/img/grenade.jpg").to be true
	end

	it 'copies all assets to ouput assets and overwrites existing assets' do
		FileUtils.mkdir_p "#{@out_dir}/assets/img"
		FileUtils.cp_r "#{@in_dir}/assets/img/carrot.png", "#{@out_dir}/assets/img/carrot.png"
		d = CarrotGrenade::Deck.new card: "#{@in_dir}/basic.html", output: @out_dir
		expect(File.exist? "#{@out_dir}/assets/img/carrot.png").to be true
		expect(File.exist? "#{@out_dir}/assets/img/grenade.jpg").to be true
	end

	after(:all) do
		FileUtils.rm_rf @out_dir, secure: true
	end

end